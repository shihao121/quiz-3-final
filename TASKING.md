## story 1
#### backend:
1. create commodity entity and database table
2. init some commodity message
3. give an interface to get commodity message
#### frontend
1. create homepage;
2. create store page display commodity message

## story 2
#### backend:
1. create order state and order count in commodity
2. alter table commodity order state and order count by flyway
2. give an interface to get order list
3. give an interface to add an order
4. give an interface to delete an order
##### frontend:
1. create an order list page
2. give an delete button to delete order
3. give commodity list page an add order button add an order
## story 3
#### backend:
1. give an interface to add a new commodity
2. add request commodity parameter valid
3. when commodity already exist return bad request with hint message
#### frontend:
1. give an add commodity page

