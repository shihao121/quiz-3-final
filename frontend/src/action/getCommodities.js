function getCommodities() {
  fetch("http://localhost:8080/api/commodities", {
    method: "GET",
  }).then(response => response.json()).then(result => {
    return result;
  });
}

export default getCommodities();