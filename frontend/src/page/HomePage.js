import React, {Component} from 'react';
import {BrowserRouter as Router, Link, NavLink, Route, Switch} from "react-router-dom";
import CommodityDetails from "../component/CommodityDetails";
import OrderDetails from "../component/OrderDetails";
import CommodityAdd from "../component/CommodityAdd";
import Footer from "../component/Footer";

class HomePage extends Component {
  render() {
    return (
      <div>
        <Router>
          <nav className="header-nav">
            <NavLink exact to="/" activeClassName="active-link">商城</NavLink>
            <NavLink to="/order" activeClassName="active-link">订单</NavLink>
            <NavLink to="/commodity/add" activeClassName="active-link">添加商品</NavLink>
          </nav>
          <Route exact path="/" component={CommodityDetails}/>
          <Route path="/order" component={OrderDetails}/>
          <Route path="/commodity/add" component={CommodityAdd}/>
        </Router>
        <Footer />
      </div>
    );
  }
}

export default HomePage;