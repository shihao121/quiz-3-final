import React, {Component} from 'react';
import HomePage from "./page/HomePage";
import "./App.less"

class App extends Component {

  render() {
    return (
      <div className="app">
        <HomePage />
      </div>)
  }
}

export default App;