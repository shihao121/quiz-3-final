import {applyMiddleware, compose, createStore} from "redux";
import reducers from "./reducer";
import thunk from "redux-thunk";

// const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
  reducers,
  compose(
    applyMiddleware(thunk),
    typeof window === 'object' &&
    typeof window.__REDUX_DEVTOOLS_EXTENSION__ !== "undefined" ?
      window.__REDUX_DEVTOOLS_EXTENSION__() :
      f => f
  ));

export default store;