const commodityReducer = (state = {}, action) => {
  if (action.type === "GET_COMMODITY_DATA") {
    return {
      ...state,
      commodities: action.payload
    };
  } else {
    return state;
  }
};

export default commodityReducer;