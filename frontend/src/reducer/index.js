import {combineReducers} from "redux";
import commodityReducer from "./commodityReducer";

const reducers = combineReducers({commodityReducer: commodityReducer});

export default reducers;