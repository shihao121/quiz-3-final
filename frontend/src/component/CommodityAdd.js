import React, {Component} from 'react';

class CommodityAdd extends Component {
  render() {
    return (
      <article className='commodity-add'>
        <h1>添加商品</h1>
        名称<br/>
        <label><input type="text"/></label><br/>
        价格<br/>
        <label><input type="text"/></label><br/>
        单位<br/>
        <label><input type="text"/></label><br/>
        图片<br/>
        <label><input type="text"/></label><br/>
        <button className="submit-button" type='submit'>提交</button>
      </article>
    );
  }
}

export default CommodityAdd;