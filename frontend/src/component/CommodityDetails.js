import React, {Component} from 'react';
import getCommodities from "../action/getCommodities";

class CommodityDetails extends Component {

  constructor(props) {
    super(props);
    this.state = {commodities: {}};
    fetch("http://localhost:8080/api/commodities", {
      method: "GET",
    }).then(response => response.json()).then(result => {
      this.setState({commodities: result});
    });
  }

  componentDidMount() {
  }

  render() {
    return (
      <article className='commodity-list'>
        {this.state.commodities !== {} ? Object.values(this.state.commodities).map(commodity => {
          return (
            <div className="commodity-details">
              <img src={commodity.url} alt={commodity.name}/><br />
              {commodity.name}<br/>
              单价：{commodity.price}元/{commodity.unit}<br />

            </div>
          );
        }) : ""}
      </article>
    );
  }
}


export default CommodityDetails;