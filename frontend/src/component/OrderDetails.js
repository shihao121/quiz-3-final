import React, {Component} from 'react';

class OrderDetails extends Component {

  constructor(props) {
    super(props);
    this.state = {orders: {}};
    fetch("http://localhost:8080/api/orders", {
      method: "GET",
    }).then(response => response.json()).then(result => {
      this.setState({orders: result});
    });
  }


  render() {
    return (
      <article className="orders">
        <table className='order-table'>
          <tr>
            <th>名字</th>
            <th>单价</th>
            <th>数量</th>
            <th>单位</th>
            <th>操作</th>
          </tr>
          {this.state.orders !== {} ? Object.values(this.state.orders).map(order => {
            return (
              <tr className="order-details">
                <td>{order.name}</td>
                <td>{order.price}</td>
                <td>{order.count}</td>
                <td>{order.unit}</td>
                <td>
                  <button className='delete-button' onClick={this.deleteOrder(order.name)}>删除</button>
                </td>
              </tr>
            );
          }) : ""}
        </table>
      </article>
    );
  }

  deleteOrder(id) {
    return undefined;
  }
}

export default OrderDetails;