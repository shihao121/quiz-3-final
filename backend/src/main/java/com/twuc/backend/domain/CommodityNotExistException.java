package com.twuc.backend.domain;

public class CommodityNotExistException extends RuntimeException {

    CommodityNotExistException(String message) {
        super(message);
    }
}
