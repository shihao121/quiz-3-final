package com.twuc.backend.domain;

import javax.persistence.*;

@Entity
public class Commodity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private Double price;

    @Column(nullable = false)
    private String unit;

    @Column(nullable = false)
    private String url;

    @Column
    private Boolean orderState;

    @Column
    private Integer orderCount;

    void setOrderState(Boolean orderState) {
        this.orderState = orderState;
    }

    void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }

    Boolean getOrderState() {
        return orderState;
    }

    public Integer getOrderCount() {
        return orderCount;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    Double getPrice() {
        return price;
    }

    String getUnit() {
        return unit;
    }

    String getUrl() {
        return url;
    }

    public Commodity(String name, Double price, String unit, String url) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.url = url;
    }

    public Commodity() {
    }
}
