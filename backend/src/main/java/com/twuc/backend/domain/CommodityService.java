package com.twuc.backend.domain;

import com.twuc.backend.contract.CommodityRequest;
import com.twuc.backend.contract.CommodityResponse;
import com.twuc.backend.contract.OrderResponse;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CommodityService {

    private CommodityRepository commodityRepository;

    public CommodityService(CommodityRepository commodityRepository) {
        this.commodityRepository = commodityRepository;
    }

    public List<CommodityResponse> findAll() {
        List<Commodity> commodities = commodityRepository.findAll();
        List<CommodityResponse> commodityResponses = new ArrayList<>();
        commodities.forEach(commodity -> {
            commodityResponses.add(new CommodityResponse(commodity.getId(),
                    commodity.getName(), commodity.getPrice(), commodity.getUnit(), commodity.getUrl()));
        });
        return commodityResponses;
    }

    public void addOrder(Long commodityId) {
        Commodity fetchedCommodity = validAndGetCommodity(commodityId);
        if (fetchedCommodity.getOrderState() == null) {
            fetchedCommodity.setOrderState(true);
            fetchedCommodity.setOrderCount(1);
        } else {
            fetchedCommodity.setOrderCount(fetchedCommodity.getOrderCount() + 1);
        }
        commodityRepository.save(fetchedCommodity);
    }

    private Commodity validAndGetCommodity(Long commodityId) {
        Optional<Commodity> commodityOptional = commodityRepository.findById(commodityId);
        if (!commodityOptional.isPresent()) {
            throw new CommodityNotExistException("commodity not exist with id" + commodityId);
        }
        return commodityOptional.get();
    }

    public List<OrderResponse> findOrders() {
        List<Commodity> commodities = commodityRepository.findByOrderStateTrue();
        List<OrderResponse> orders = new ArrayList<>();
        commodities.forEach(commodity -> {
            orders.add(new OrderResponse(commodity.getName(), commodity.getOrderCount(),
                    commodity.getPrice(), commodity.getUnit()));
        });
        return orders;
    }

    public void deleteOrder(Long commodityId) {
        Commodity fetchedCommodity = validAndGetCommodity(commodityId);
        fetchedCommodity.setOrderCount(0);
        fetchedCommodity.setOrderState(false);
        commodityRepository.save(fetchedCommodity);
    }

    public void save(CommodityRequest commodityRequest) {
        Optional<Commodity> fetchedCommodityOptional = commodityRepository.findByName(commodityRequest.getName());
        if (fetchedCommodityOptional.isPresent()) {
            throw new IllegalArgumentException("commodity already exist");
        }
        commodityRepository.save(new Commodity(commodityRequest.getName(), commodityRequest.getPrice()
                , commodityRequest.getUnit(), commodityRequest.getUrl()));
    }
}
