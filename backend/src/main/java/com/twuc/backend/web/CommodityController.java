package com.twuc.backend.web;

import com.twuc.backend.contract.CommodityRequest;
import com.twuc.backend.contract.CommodityResponse;
import com.twuc.backend.contract.OrderResponse;
import com.twuc.backend.domain.CommodityNotExistException;
import com.twuc.backend.domain.CommodityService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/api")
@CrossOrigin("*")
public class CommodityController {

    private CommodityService commodityService;

    public CommodityController(CommodityService commodityService) {
        this.commodityService = commodityService;
    }

    @GetMapping(value = "/commodities")
    public ResponseEntity<List<CommodityResponse>> getCommodities() {
        List<CommodityResponse> commodities = commodityService.findAll();
        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(commodities);
    }

    @PostMapping(value = "/commodities/{commodityId}")
    public ResponseEntity addOrder(@PathVariable Long commodityId) {
        try {
            commodityService.addOrder(commodityId);
            return ResponseEntity.ok().build();
        } catch (CommodityNotExistException exception) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @GetMapping(value = "/orders")
    public ResponseEntity<List<OrderResponse>> getOrders() {
        List<OrderResponse> orders = commodityService.findOrders();
        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON)
                .body(orders);
    }

    @DeleteMapping(value = "/orders/{commodityId}")
    public ResponseEntity deleteOrder(@PathVariable Long commodityId) {
        try {
            commodityService.deleteOrder(commodityId);
            return ResponseEntity.ok().build();
        } catch (CommodityNotExistException exception) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @PostMapping(value = "/commodities")
    public ResponseEntity addCommodities(@RequestBody @Valid CommodityRequest commodityRequest) {

        try {
            commodityService.save(commodityRequest);
        } catch (IllegalArgumentException exception) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
        }
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}
