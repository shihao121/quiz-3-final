package com.twuc.backend.web;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@CrossOrigin("*")
public class TestController {

    @GetMapping("test")
    public Map<String, String> test() {
        HashMap<String, String> map = new HashMap<>();
        map.put("test", "test");
        return map;
    }
}
