package com.twuc.backend.contract;

public class CommodityResponse {

    private Long id;

    private String name;

    private Double price;

    private String unit;

    private String url;

    public CommodityResponse(Long id, String name, Double price, String unit, String url) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.url = url;
    }

    public Long getId() {
        return id;
    }

    public CommodityResponse() {
    }

    public String getName() {
        return name;
    }

    public Double getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getUrl() {
        return url;
    }
}
