package com.twuc.backend.contract;

public class OrderResponse {

    private String name;

    private Integer count;

    private Double price;

    private String unit;

    public OrderResponse() {
    }

    public String getName() {
        return name;
    }

    public Integer getCount() {
        return count;
    }

    public Double getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public OrderResponse(String name, Integer count, Double price, String unit) {
        this.name = name;
        this.count = count;
        this.price = price;
        this.unit = unit;
    }
}
