package com.twuc.backend.contract;

import javax.validation.constraints.NotNull;

public class CommodityRequest {

    @NotNull
    private String name;

    @NotNull
    private Double price;

    @NotNull
    private String unit;

    @NotNull
    private String url;

    public CommodityRequest(String name, Double price, String unit, String url) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public Double getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getUrl() {
        return url;
    }

    public CommodityRequest() {
    }
}
