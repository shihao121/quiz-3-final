CREATE TABLE IF NOT EXISTS `commodity`
(
    `id`    BIGINT PRIMARY KEY AUTO_INCREMENT,
    `name`  VARCHAR(64)  NOT NULL,
    `price` DOUBLE       NOT NULL,
    `unit`  VARCHAR(10)  NOT NULL,
    `url`   VARCHAR(128) NOT NULL
) CHAR SET utf8 COLLATE utf8_unicode_ci;
