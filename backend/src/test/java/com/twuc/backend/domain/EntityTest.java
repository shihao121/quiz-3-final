package com.twuc.backend.domain;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class EntityTest {

    @Autowired
    private CommodityRepository commodityRepository;

    @Autowired
    private EntityManager entityManager;

    @Test
    void should_save_and_get_commodity_info() {

        Commodity commodity = commodityRepository.save(new Commodity("可乐", (double) 12, "瓶", "http"));

        entityManager.flush();
        entityManager.clear();
        Optional<Commodity> fetchedCommodityOptional = commodityRepository.findById(commodity.getId());
        assertTrue(fetchedCommodityOptional.isPresent());
        assertEquals("可乐", fetchedCommodityOptional.get().getName());
    }
}