package com.twuc.backend.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.backend.contract.CommodityRequest;
import com.twuc.backend.domain.Commodity;
import com.twuc.backend.domain.CommodityRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class CommodityControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private CommodityRepository commodityRepository;

    private static final int INIT_COMMODITY_COUNT = 2;

    @BeforeEach
    void initDate() {
        List<Commodity> commodities = new ArrayList<>();
        commodities.add(new Commodity("T-shirt", (double) 80, "pieces",
                "https://static.gracegift.com.tw/upload/category_small_img/e9847887f7ba3906378eea91573f66c6/1563194405.jpg?t=1563194405"));
        commodities.add(new Commodity("iphone", (double) 6666, "volume", "https://www.costco.com.tw/medias/sys_master/images/h18/h70/11354215907358.jpg"));
        commodityRepository.saveAll(commodities);
    }

    @Test
    void should_get_commodities() throws Exception {
        mockMvc.perform(get("/api/commodities"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$[0].name").exists());
    }

    @Test
    void should_add_a_order() throws Exception {
        mockMvc.perform(post("/api/commodities/1"))
                .andExpect(status().isOk());
        Optional<Commodity> fetchedCommodityOptional = commodityRepository.findById(1L);
        assertTrue(fetchedCommodityOptional.isPresent());
        assertEquals(1, (int)fetchedCommodityOptional.get().getOrderCount());
    }

    @Test
    void should_get_orders() throws Exception {
        mockMvc.perform(post("/api/commodities/1"))
                .andExpect(status().isOk());
        mockMvc.perform(get("/api/orders"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$[0].name").value("T-shirt"));
    }

    @Test
    void should_delete_order() throws Exception {
        addToOrders();
        mockMvc.perform(get("/api/orders"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$[1]").exists());
        mockMvc.perform(delete("/api/orders/1"))
                .andExpect(status().isOk());
        mockMvc.perform(get("/api/orders"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$[1]").doesNotExist());
    }

    @Test
    void should_add_commodity() throws Exception {
        String commodityRequest = objectMapper.writeValueAsString(new CommodityRequest("basketball", 240D, "piece", "fake-url"));
        mockMvc.perform(post("/api/commodities")
                .contentType(MediaType.APPLICATION_JSON)
                .content(commodityRequest))
                .andExpect(status().isOk());
        mockMvc.perform(get("/api/commodities"))
                .andExpect(jsonPath(String.format("$[%d]", INIT_COMMODITY_COUNT)).exists());
    }

    private void addToOrders() throws Exception {
        mockMvc.perform(post("/api/commodities/1"))
                .andExpect(status().isOk());
        mockMvc.perform(post("/api/commodities/2"))
                .andExpect(status().isOk());
    }
}